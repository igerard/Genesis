//
//  GenesisTests.swift
//  GenesisTests
//
//  Created by Gerard Iglesias on 27/08/2017.
//  Copyright © 2017 Gerard Iglesias. All rights reserved.
//

import XCTest
import simd
@testable import Genesis

class GenesisTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
  
  func testFingerGeometryDecodable() {
    let fingerTouch = TouchGeometry.finger(pos: float2(1,2))
    let data = try? JSONEncoder().encode(fingerTouch)
    let touch = try? JSONDecoder().decode(TouchGeometry.self, from: data!)
    switch touch {
    case .none :
      XCTFail("bad decoding sequence")
    case .some(.finger(let pos)) :
      XCTAssertEqual(pos.x, Float(1), "Bad pos X")
      XCTAssertEqual(pos.y, Float(2), "Bad pos Y")
    case .some(.pencil(_, _, _, _, _, _)):
      XCTFail("A pencile geoemtry ?")
    }
  }
  
  func testPencilGeometryDecodable() {
    let pencilTouch = TouchGeometry.pencil(pos: float2(1,2), force: 0.5, altAngle: 1.2, azimAngle: 1.7, azimVector: float2(0.3,0.2), estimateIndex: .none)
    let data = try? JSONEncoder().encode(pencilTouch)
    let touch = try? JSONDecoder().decode(TouchGeometry.self, from: data!)
    switch touch {
    case .none :
      XCTFail("bad decoding sequence")
    case .some(.finger(_)) :
      XCTFail()
    case .some(.pencil(let pos, let force, let altAngle, let azimAngle, let azimVector, let ui)):
      XCTAssertEqual(pos.x, Float(1), "Bad pos X")
      XCTAssertEqual(pos.y, Float(2), "Bad pos Y")
      XCTAssertEqual(force, Float(0.5), "Bad force")
      XCTAssertEqual(altAngle, Float(1.2), "Bad altAngle")
      XCTAssertEqual(azimAngle, Float(1.7), "Bad azimAngle")
      XCTAssertEqual(azimVector.x, Float(0.3), "Bad azim vector x")
      XCTAssertEqual(azimVector.y, Float(0.2), "Bad azim vector Y")
      XCTAssertEqual(ui, .none, "Bad update index")
    }
  }
  
  func testEncodingArray() {
    let array: [[Float]] = [[1,2],[3,4,5],[6,7,8,9]]
    if let data = try? JSONEncoder().encode(array){
      let darray = try? JSONDecoder().decode([[Float]].self, from: data)
      XCTAssertEqual(darray?.count, 3, "Array not right size")
      XCTAssertEqual(darray![0].count, 2, "Array[0] not right size")
      XCTAssertEqual(darray![1].count, 3, "Array[1] not right size")
      XCTAssertEqual(darray![2].count, 4, "Array[2] not right size")
    } else {
      XCTFail()
    }
  }
  
}
