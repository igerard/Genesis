//: [Previous](@previous)

import Foundation

struct InfoPoint {
  let pos : CGPoint
  let force : Float
}

var object : InfoPoint? = nil

object.map{ ip in
  print(ip.pos.debugDescription)
  print(ip.force.debugDescription)
}

object = InfoPoint(pos: CGPoint(x: 1, y: 2), force: 4.6)

object.map{ ip in
  print(ip.pos.debugDescription)
  print(ip.force.debugDescription)
}

//: [Next](@next)
