//: A Cocoa based Playground to present user interface

import AppKit
import PlaygroundSupport
import simd

//let nibFile = NSNib.Name("MyView")
//var topLevelObjects : NSArray?
//
//Bundle.main.loadNibNamed(nibFile, owner:nil, topLevelObjects: &topLevelObjects)
//let views = (topLevelObjects as! Array<Any>).filter { $0 is NSView }
//
//// Present the view in Playground
//PlaygroundPage.current.liveView = views[0] as! NSView
//
//PlaygroundPage.current.liveView

let mt = float4x4(float4(1, 0, 0, 0), float4(0, 2, 0, 0.0), float4(0.0, 0, 3, 0.0), float4(2, 3, 4, 1))

(mt * float4(x:1.0, y:2.0, z:3.0, w:1.0)).debugDescription

