//: [Previous](@previous)

import Foundation

extension Int {
  func apply<R>(f: (Int) -> R) {
    f(self)
  }
}

3.apply{ $0 == 0 ? 1 : -1 }


let a: Int? = nil
a.debugDescription

struct Data {
  var a: Float
  var b: String
}

var table = [Data(a: 3, b: "Hello"), Data(a: 5, b: "World")]

table[1] = Data(a: -78, b: "Bad")

table[1]

var dict = [23: table[0], 56: table[1]]

dict[23] = Data(a: 5, b: "Girl")

var item = dict[56]
item?.a = 7

dict
table
item

//: [Next](@next)
