//
//  GeoAxes.swift
//  Genesis
//
//  Created by Gerard Iglesias on 13/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import Foundation
import Metal
import MetalKit
import simd

class GeoAxes: Geometry {

  var position = float2(x: 0, y:0) {
    didSet {
      updateBuffer()
    }
  }
  var size = vector_float2(x: 1, y: 1) {
    didSet {
      updateBuffer()
    }
  }
  fileprivate var vertices = [Vertex](repeating: vertex_neutral, count: 4)
  fileprivate var vertexBuffer : MTLBuffer?

  init?(device: MTLDevice?) {
    guard device != nil else {
      return nil
    }
    vertices[0].color.x = 1
    vertices[0].color.y = 1
    vertices[1].color.x = 1
    vertices[2].color.y = 1
    vertices[0].color.z = 1
    vertices[3].color.z = 1
    vertexBuffer = device?.makeBuffer(bytes: vertices, length: vertices.count*MemoryLayout<Vertex>.stride, options: .storageModeShared)

    guard vertexBuffer != nil else {
      return nil
    }
  }

  func draw(encoder: MTLRenderCommandEncoder, viewingBuffer: MTLBuffer) {
    encoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
    encoder.setVertexBuffer(viewingBuffer, offset: 0, index: 1)
    encoder.drawPrimitives(type: .line, vertexStart: 0, vertexCount: vertexBuffer!.length / MemoryLayout<Vertex>.stride)
  }

  fileprivate func updateBuffer() {
    vertices[0].position.x = position.x - size.x
    vertices[0].position.y = position.y
    vertices[1].position.x = position.x + size.x
    vertices[1].position.y = position.y
    vertices[2].position.x = position.x
    vertices[2].position.y = position.y - size.y
    vertices[3].position.x = position.x
    vertices[3].position.y = position.y + size.y
    memcpy(vertexBuffer?.contents(), vertices, vertices.count * MemoryLayout<Vertex>.size)
  }

}
