//
//  GeoPointInfo.swift
//  Genesis
//
//  Created by Gerard Iglesias on 26/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import Foundation
import Metal
import MetalKit
import simd

class GeoPointInfo: Geometry {

  var infoPoint: TouchGeometry! {
    didSet {
      updateBuffer()
    }
  }
  fileprivate var vertices = [Vertex](repeating: vertex_neutral, count: 6)
  fileprivate var isPencil = true
  fileprivate let vertexBuffer : MTLBuffer?

  init?(info: TouchGeometry, device: MTLDevice?) {

    guard device != nil else {
      return nil
    }
    infoPoint = info
    // set the color of the vertices
    vertices[0].color = float4([1,1,0,1])
    vertices[1].color = float4([1,0,0,1])
    vertices[2].color = float4([0,1,1,1])
    vertices[3].color = float4([0,0,1,1])
    vertices[4].color = float4([1,1,1,1])
    vertices[5].color = float4([0,0,0,1])
    vertexBuffer = device?.makeBuffer(bytes: vertices, length: vertices.count*MemoryLayout<Vertex>.stride, options: .storageModeShared)
    updateBuffer()

    guard vertexBuffer != nil else {
      return nil
    }
  }

  func draw(encoder: MTLRenderCommandEncoder, viewingBuffer: MTLBuffer) {
    encoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
    encoder.setVertexBuffer(viewingBuffer, offset: 0, index: 1)
    encoder.drawPrimitives(type: .line, vertexStart: 0, vertexCount: (isPencil ? vertices.count : 4))
  }

  fileprivate func updateBuffer() {
    // compute points from the
    var position: float2
    switch infoPoint! {
    case .pencil(let pos, let force, let altAngle, _, let azimVector, _):
      isPencil = true
      position = pos
      vertices[0].position = float4([position.x-force,position.y,0,1])
      vertices[1].position = float4([position.x+force,position.y,0,1])
      vertices[2].position = float4([position.x,position.y-force,0,1])
      vertices[3].position = float4([position.x,position.y+force,0,1])
      vertices[4].position = float4([position.x,position.y,0,1])
      vertices[5].position = float4([position.x+azimVector.x/altAngle,position.y+azimVector.y/altAngle,0,1])
    case .finger(let pos):
      isPencil = false
      position = pos
      vertices[0].position = float4([position.x-1,position.y,0,1])
      vertices[1].position = float4([position.x+1,position.y,0,1])
      vertices[2].position = float4([position.x,position.y-1,0,1])
      vertices[3].position = float4([position.x,position.y+1,0,1])
      break
    }
    memcpy(vertexBuffer?.contents(), vertices, vertices.count * MemoryLayout<Vertex>.size)
  }
}

