//
//  GoeGridPage.swift
//  Genesis
//
//  Created by Gerard Iglesias on 04/11/2017.
//  Copyright © 2017 Gerard Iglesias. All rights reserved.
//

import Foundation
import Metal
import MetalKit
import simd

class GeoPageGrid: Geometry {

  let format: PageFormat
  let vertexBuffer : MTLBuffer?

  init?(pageFormat: PageFormat, device: MTLDevice?) {

    guard device != nil else {
      return nil
    }

    format = pageFormat
    let median = (Int(pageFormat.width / 2), Int(pageFormat.height / 2))
    let vertices : [Vertex] = [
      // X constant
      Vertex(position: float4(0.0, 0.0, 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),
      Vertex(position: float4(0.0, Float(format.height), 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),

      Vertex(position: float4(Float(median.0), 0.0, 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),
      Vertex(position: float4(Float(median.0), Float(format.height), 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),

      Vertex(position: float4(Float(format.width), Float(format.height), 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),
      Vertex(position: float4(Float(format.width), 0.0, 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),

      // Y constant
      Vertex(position: float4(0.0, Float(format.height), 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),
      Vertex(position: float4(Float(format.width), Float(format.height), 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),

      Vertex(position: float4(0.0, Float(median.1), 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),
      Vertex(position: float4(Float(format.width), Float(median.1), 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),

      Vertex(position: float4(0.0, 0.0, 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),
      Vertex(position: float4(Float(format.width), 0.0, 0.0, 1), color: float4(0.9, 0.9, 0.9, 1)),
    ]
    vertexBuffer = device?.makeBuffer(bytes: vertices, length: vertices.count*MemoryLayout<Vertex>.stride, options: .storageModeShared)

    guard vertexBuffer != nil else {
      return nil
    }
  }

  public func draw(encoder: MTLRenderCommandEncoder, viewingBuffer: MTLBuffer) {
    encoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
    encoder.setVertexBuffer(viewingBuffer, offset: 0, index: 1)
    encoder.drawPrimitives(type: .line, vertexStart: 0, vertexCount: vertexBuffer!.length / MemoryLayout<Vertex>.stride)
  }

  
}
