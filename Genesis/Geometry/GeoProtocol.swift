//
//  GeoProtocol.swift
//  Genesis
//
//  Created by Gerard Iglesias on 05/11/2017.
//  Copyright © 2017 Gerard Iglesias. All rights reserved.
//

import Foundation
import Metal
import MetalKit
import simd

/// Define behaviour for an object that draw itself inside a Metal context
protocol Geometry {
  /// Draw the geometry given a buffer containing the view transformation
  ///
  /// - Parameters:
  ///   - encoder: The Metal commad encoder to use
  ///   - viewingBuffer: the viewing transformation
  func draw(encoder: MTLRenderCommandEncoder, viewingBuffer: MTLBuffer)
}
