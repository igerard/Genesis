//
//  GeoLines.swift
//  Genesis
//
//  Created by Gerard Iglesias on 16/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import Foundation
import Metal
import MetalKit
import simd

/// The Vertex structure
struct SimpleVertex {
  var position : float4
}

struct Vertex {
  var position : float4
  var color : float4
}

let simple_vertex_neutral = SimpleVertex(position: float4([0,0,0,1]))

let vertex_neutral = Vertex(position: float4([0,0,0,1]), color: float4([0,0,0,1]))

class GeoLine: Geometry {

  fileprivate var sampling : [TouchGeometry]!
  fileprivate var vertexBuffer : MTLBuffer!

  init?(device: MTLDevice?, geometry: [TouchGeometry]) {
    guard device != nil else {
      return nil
    }
    sampling = geometry
    vertexBuffer = device?.makeBuffer(length: geometry.count*MemoryLayout<SimpleVertex>.stride, options: .storageModeShared)
    guard vertexBuffer != nil else {
      return nil
    }
    let vertices = vertexBuffer.contents().bindMemory(to: SimpleVertex.self, capacity: geometry.count)
    for i in 0..<sampling.count {
      let vertex = sampling[i]
      switch vertex {
      case .finger(let pos): vertices[i].position = float4(pos.x, pos.y, 0, 1)
      case .pencil(let pos, _, _, _, _, _): vertices[i].position = float4(pos.x, pos.y, 0, 1)
      }
    }
  }

  func draw(encoder: MTLRenderCommandEncoder, viewingBuffer: MTLBuffer) {
    encoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
    encoder.setVertexBuffer(viewingBuffer, offset: 0, index: 1)
    encoder.drawPrimitives(type: .lineStrip, vertexStart: 0, vertexCount: vertexBuffer!.length / MemoryLayout<SimpleVertex>.stride)
  }

}
