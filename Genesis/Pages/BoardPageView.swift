//
//  BoardPageView.swift
//  Genesis
//
//  Created by Gerard Iglesias on 01/10/2017.
//  Copyright © 2017 Gerard Iglesias. All rights reserved.
//

import UIKit
import Metal
import MetalKit
import simd
import os
import CoreData

struct Uniforms
{
  var modelViewProjectionMatrix : matrix_float4x4
};

class BoardPageView: MTKView {

  let id: String
  
  var container: BoardContainer? {
    didSet {
      loadContent()
    }
  }
  // page storage
  var boardPage: BoardPage? = nil

  override var frame: CGRect {
    didSet {
      updateMatrix()
    }
  }

  var pageFormat = A4 {
    didSet {
      camera = BoardPageCamera(center: CGPoint(x: pageFormat.width/2.0, y: pageFormat.height/2.0), up: CGVector(dx: 0, dy: 1), extend: CGSize(width: pageFormat.width, height: pageFormat.height))
      updateMatrix()
      setNeedsDisplay()
    }
  }

  var showGrid = false {
    didSet {
      setNeedsDisplay()
      NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":showGrid ? "Show Grid" : "Hide Grid"])
    }
  }
  
  var showInfoPoint = false {
    didSet {
      setNeedsDisplay()
      NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":showInfoPoint ? "Show Info Point" : "Hide InfoPoint"])
    }
  }
  
  var mlLayer : CAMetalLayer {
    get {
      return self.layer as! CAMetalLayer
    }
  }
  
  var commandQueue: MTLCommandQueue?
  
  // MARK Data structure

  // viewing of the world
  var camera: BoardPageCamera!;
  // the struc to pass transformation to the shader
  var uniforms = Uniforms(modelViewProjectionMatrix: matrix_identity_float4x4)
  var inversMapping: float4x4!

  let pageGrid: GeoPageGrid!
  let axes: GeoAxes!
  let infoPoint: GeoPointInfo!
  var viewingBuffer : MTLBuffer?
  
  var library : MTLLibrary?
  var vertexFunction : MTLFunction?
  var fragmentFunction : MTLFunction?
  var simpleVertexFunction : MTLFunction?
  var simpleFragmentFunction : MTLFunction?

  var polyLine = [TouchGeometry]()
  var lines = [GeoLine]()

  // pipelines
  var simpleVertexPipelineState : MTLRenderPipelineState?
  var vertexPipelineState : MTLRenderPipelineState?

  // Temporary line
  var bufferVerticeCapacity = 100
  var lineBuffer : MTLBuffer!
  var bufferVertices : UnsafeMutablePointer<SimpleVertex>!

  // MARK: Init Phase
  
  init(id: String, frame frameRect: CGRect, device: MTLDevice?) {

    self.id = id
    camera = BoardPageCamera(center: CGPoint(x: pageFormat.width/2.0, y: pageFormat.height/2.0), up: CGVector(dx: 0, dy: 1), extend: CGSize(width: pageFormat.width, height: pageFormat.height))

    axes = GeoAxes(device: device)
    infoPoint = GeoPointInfo(info: .finger(pos: float2(0,0)), device: device)
    pageGrid = device.flatMap{GeoPageGrid(pageFormat: A4, device: $0)}

    super.init(frame: frameRect, device: device)

    isOpaque = false
    mlLayer.isOpaque = false

    let value = 1.0
    clearColor = MTLClearColor(red: value, green: value, blue: value, alpha: 1)
    clearDepth = 1.0
    clearStencil = 0
    colorPixelFormat = .bgra8Unorm
    sampleCount = 4
    isPaused = true
    enableSetNeedsDisplay = true
    
    commandQueue = self.device?.makeCommandQueue()
    
    makeBuffers()
    makePipelines()
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func loadContent() {
    if let boardContainer = container {
      // load geometry from the container with a fetch request
      let request = NSFetchRequest<BoardPage>(entityName: "BoardPage")
      let predicate = NSPredicate(format: "id == %@", self.id)
      request.predicate = predicate
      // extract the page if it exists
      do {
        let result = try boardContainer.viewContext.fetch(request)
        if result.count == 1 {
          boardPage = result[0]
          // read all the sampling and add it to the live session
          boardPage!.geometries?.forEach { item in
            if let geometries = item as? TouchGeometries {
              let arrays = try! JSONDecoder().decode([[Float]].self, from: geometries.sampling!)
              let touches: [TouchGeometry] = arrays.map{ array in
                if array.count == 2 {
                  return .finger(pos: float2(array[0], array[1]))
                } else {
                  return .pencil(pos: float2(array[0],array[1]), force: array[2], altAngle: array[3], azimAngle: array[4], azimVector: float2(array[5],array[6]), estimateIndex: .none)
                }
              }
              GeoLine(device: device, geometry: touches).map{lines.append($0)}
            }
          }
          setNeedsDisplay()
          NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"Page \(id) loaded from CoreData"])
          NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"\(lines.count) lines loaded"])
        } else if result.count == 0 {
          // if not found add one
          boardPage = BoardPage(context: boardContainer.viewContext)
          boardPage?.id = id
          boardContainer.saveContextIfNeeded()
        } else {
          fatalError("More than one page with id = \(id)")
        }
      }
      catch {
        fatalError("Something very wrong with CoreData")
      }
    }
  }
  
  func makeBuffers(){
    // the viewing buffer
    let viewingBufferSize = MemoryLayout<Uniforms>.size
    viewingBuffer = self.device?.makeBuffer(length: viewingBufferSize, options: .cpuCacheModeWriteCombined)
    
    lineBuffer = device?.makeBuffer(length: bufferVerticeCapacity*MemoryLayout<Vertex>.stride, options: .cpuCacheModeWriteCombined)
    guard lineBuffer != nil else {
      fatalError("makeBuffers failure")
    }
    bufferVertices = lineBuffer.contents().bindMemory(to: SimpleVertex.self, capacity: bufferVerticeCapacity)
  }
  
  func makePipelines(){
    library = self.device?.makeDefaultLibrary()
    vertexFunction = library?.makeFunction(name : "vertex_main")
    fragmentFunction = library?.makeFunction(name : "fragment_main")
    
    let vertexColorPipelineDescr = MTLRenderPipelineDescriptor()
    vertexColorPipelineDescr.vertexFunction = vertexFunction
    vertexColorPipelineDescr.fragmentFunction = fragmentFunction
    vertexColorPipelineDescr.colorAttachments[0].pixelFormat = mlLayer.pixelFormat
    vertexColorPipelineDescr.sampleCount = sampleCount
    
    do {
      vertexPipelineState = try device!.makeRenderPipelineState(descriptor: vertexColorPipelineDescr)
    }
    catch {
      //os_log("%@", log: boardLog, type: .error, error.localizedDescription)
    }

    simpleVertexFunction = library?.makeFunction(name : "simple_vertex_main")
    simpleFragmentFunction = library?.makeFunction(name : "simple_fragment_main")
    let simpleVertexColorPipelineDescr = MTLRenderPipelineDescriptor()
    simpleVertexColorPipelineDescr.vertexFunction = simpleVertexFunction
    simpleVertexColorPipelineDescr.fragmentFunction = simpleFragmentFunction
    simpleVertexColorPipelineDescr.colorAttachments[0].pixelFormat = mlLayer.pixelFormat
    simpleVertexColorPipelineDescr.sampleCount = sampleCount
    
    do {
      simpleVertexPipelineState = try device!.makeRenderPipelineState(descriptor: simpleVertexColorPipelineDescr)
    }
    catch {
      //os_log("%@", log: boardLog, type: .error, error.localizedDescription)
    }
  }
  
  // refresh the viewing transformation
  fileprivate func updateMatrix() {
    uniforms.modelViewProjectionMatrix = camera.metalProjectionToViewport(bounds.size)
    inversMapping = camera.viewProjectionToViewport(bounds.size).inverse
  }

  // MARK: Display
  
  override func draw(_ rect : CGRect) {
    guard self.device != nil else{
      //os_log("No Metal device", log: boardLog, type: .error)
      return
    }
    guard self.commandQueue != nil else{
      //os_log("No Command Queue", log: boardLog, type: .error)
      return
    }
    guard self.vertexPipelineState != nil else{
      //os_log("No pipeline", log: boardLog, type: .error)
      return
    }
    guard self.simpleVertexPipelineState != nil else{
      //os_log("No pipeline", log: boardLog, type: .error)
      return
    }
    
    if let drawable = self.currentDrawable,
      let commandBuffer = commandQueue!.makeCommandBuffer(),
      let passDescr = self.currentRenderPassDescriptor,
      let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: passDescr){
      
      commandEncoder.setRenderPipelineState(vertexPipelineState!)
      
      let nativeScale = Double(UIScreen.main.nativeScale)
      let viewport = MTLViewport(originX: 0, originY: 0, width: nativeScale*Double(bounds.width), height: nativeScale*Double(bounds.height), znear: 0, zfar: 1)
      commandEncoder.setViewport(viewport)

      // copy transformation into the dedicated buffer
      memcpy(viewingBuffer?.contents(), &uniforms, MemoryLayout<Uniforms>.size)
      // draw board geometry
      drawGeometry(commandEncoder)

      commandEncoder.endEncoding()
      commandBuffer.present(drawable)
      commandBuffer.commit()
    }
  }
  
  /// Draw the scene
  ///
  /// - Parameter commandEncoder: the Metal encoder to use for
  fileprivate func drawGeometry(_ commandEncoder: MTLRenderCommandEncoder) {
    if let viewBuffer = viewingBuffer {
      commandEncoder.setRenderPipelineState(vertexPipelineState!)
      axes.draw(encoder: commandEncoder, viewingBuffer: viewBuffer)
      if showInfoPoint { infoPoint.draw(encoder: commandEncoder, viewingBuffer: viewBuffer) }
      if showGrid { pageGrid?.draw(encoder: commandEncoder, viewingBuffer: viewBuffer) }
      if polyLine.count > 0 || lines.count > 0 {
        commandEncoder.setRenderPipelineState(simpleVertexPipelineState!)
        // draw the cretaed lines
        lines.forEach{ line in
          line.draw(encoder: commandEncoder, viewingBuffer: viewBuffer)
        }
        // copy the vertex of the last curve into the vertices
        // check if enough room
        commandEncoder.setVertexBuffer(lineBuffer, offset: 0, index: 0)
        commandEncoder.setVertexBuffer(viewingBuffer, offset: 0, index: 1)
        commandEncoder.drawPrimitives(type: .lineStrip, vertexStart: 0, vertexCount: polyLine.count)

      }
    }
  }
  
  // MARK: Events
    
  // MARK: Layouts
  
  // MARK: Archive
  
  override func awakeFromNib() {
    //os_log("%@", log: boardLog, type: .debug, self.mlLayer.debugDescription)
  }
  
  func clearPage() {
    lines.removeAll()
    // clear the data in the storage
    var nbLines = 0
    if let boardContainer = container, let page = boardPage {
      if let geometries = page.geometries {
        for item in geometries {
          if let touches = item as? TouchGeometries {
            boardContainer.viewContext.delete(touches)
            nbLines += 1
          }
        }
      }
    }
//    boardPage.map{ page in page.geometries.map{page.removeFromGeometries($0)}}
    container.map{$0.saveContextIfNeeded()}
    setNeedsDisplay()
    NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"Page \(id) cleared, \(nbLines) deleted"])
  }
  
  
  @IBAction func zoomWithPinch(_ gestureRecognizer : UIPinchGestureRecognizer) {
      switch gestureRecognizer.state {
      case .began:
        let pos = gestureRecognizer.location(in: self)
        print("Pinch at : \(pos.x) , \(pos.y)")
      case .changed:
        let pos = gestureRecognizer.location(in: self)
        print("Pinch at : \(pos.x) , \(pos.y)")
        print("With scale : \(gestureRecognizer.scale)")
        print("With velocity : \(gestureRecognizer.velocity)")
      default:
        break
      }
    }

}

// MARK: - RecognizeCurrveViewProtocol
extension BoardPageView: PolylineReceiver {

  fileprivate func addTouch(_ points: [TouchGeometry]) {
    var pointIndex = polyLine.count
    points.forEach{ point in
      if bufferVerticeCapacity < polyLine.count {
        bufferVerticeCapacity += 100
        let newLineBuffer = device?.makeBuffer(length: bufferVerticeCapacity*MemoryLayout<Vertex>.stride, options: .cpuCacheModeWriteCombined)
        guard newLineBuffer != nil else {
          fatalError("makeBuffers failure")
        }
        memcpy(newLineBuffer?.contents(), lineBuffer.contents(), lineBuffer.length)
        lineBuffer = newLineBuffer
        bufferVertices = lineBuffer.contents().bindMemory(to: SimpleVertex.self, capacity: bufferVerticeCapacity)
      }
      switch point {
      case .pencil(let pos, let force, let alt, let azim, let azimV, let index):
        let trPoint = inversMapping * float4([Float(pos.x), Float(pos.y), Float(0.0), Float(1.0)])
        polyLine.append(TouchGeometry.pencil(pos: float2(trPoint.x, trPoint.y),
                                          force: force,
                                          altAngle: alt,
                                          azimAngle: azim,
                                          azimVector: azimV,
                                          estimateIndex: index))
        bufferVertices[pointIndex] = SimpleVertex(position: float4(trPoint.x, trPoint.y, 0, 1))
        pointIndex += 1
        //os_log("point tr = %@", log: boardLog, type: .debug, trPoint.debugDescription)
        infoPoint.infoPoint = polyLine.last!
      case .finger(let pos):
        let trPoint = inversMapping * float4([Float(pos.x), Float(pos.y), Float(0.0), Float(1.0)])
        polyLine.append(TouchGeometry.finger(pos: float2(trPoint.x, trPoint.y)))
        bufferVertices[pointIndex] = SimpleVertex(position: float4(trPoint.x, trPoint.y, 0, 1))
        pointIndex += 1
        infoPoint.infoPoint = polyLine.last!
      }
    }
    setNeedsDisplay()
  }

  /// start a curve with a TouchGeometry data
  ///
  /// - Parameter point: the TouchGeometry info
  func startCurveWith(_ point: TouchGeometry) {
    polyLine.removeAll()
    addTouch([point])
  }

  func continueCurveWith(_ points: [TouchGeometry]) {
    addTouch(points)
  }

  func endCurveWith(_ points: [TouchGeometry]) {
    addTouch(points)
    NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"New Curve with \(polyLine.count) points"])

    // we add the last curve as a GeoLine
    GeoLine(device: device, geometry: polyLine).map{
      lines.append($0)
      if let boardContainer = container {
        let geometries  = TouchGeometries(context: boardContainer.viewContext)
        geometries.page = boardPage
        geometries.sampling = samplingData(line: polyLine)
        boardPage?.geometries?.adding(geometries)
        boardContainer.saveContextIfNeeded()
      }
      polyLine.removeAll()
    }
  }

  func updateGeometryWith(_ info: [TouchUpdateGeometry]) {
    // does nothing ... for now no memoization of the data
  }
  
  func samplingData(line: [TouchGeometry]) -> Data {
    let array: [[Float]] = line.map{ (touch: TouchGeometry) in
      switch touch {
      case .finger(let pos):
        return [pos.x,pos.y]
      case .pencil(let pos, let force, let alt, let azim, let azimV, _):
        return [pos.x, pos.y, force, alt, azim, azimV.x, azimV.y]
      }
    }
    return try! JSONEncoder().encode(array)
  }

}
