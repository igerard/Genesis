//
//  BoardContainer.swift
//  Genesis
//
//  Created by Iglesias, Gérard (FircoSoft) on 28/09/2017.
//  Copyright © 2017 Gerard Iglesias. All rights reserved.
//

import CoreData

class BoardContainer: NSPersistentContainer {

}

// MARK: - Core Data Saving support

extension NSPersistentContainer {
  func saveContextIfNeeded() {
    if viewContext.hasChanges {
      do {
        try viewContext.save()
      } catch {
        let nserror = error as NSError
        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
      }
    }
  }
}
