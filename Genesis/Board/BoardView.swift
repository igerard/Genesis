//
//  BoardView.swift
//  Genesis
//
//  Created by Iglesias, Gérard (FircoSoft) on 28/09/2017.
//  Copyright © 2017 Gerard Iglesias. All rights reserved.
//

import UIKit
import os

/// Will organize the layout of the document pages
class BoardView: UIView {

  var pages = [BoardPageView]()
  var current = 0
  let curveRecogniser: GeoCurveRecogniser!

  let device = MTLCreateSystemDefaultDevice()
  
  var container: BoardContainer? {
    didSet {
      // load the data for the pages on screen
      pages.forEach { $0.container = container }
    }
  }
  
  let margin = CGSize(width: 5, height: 5)

  required init?(coder aDecoder: NSCoder) {

    let firstPage = BoardPageView(id: "1,1", frame: CGRect(x: 0, y: 0, width: A4.width, height: A4.height), device: device)

    curveRecogniser = GeoCurveRecogniser(target: firstPage, action: nil)
    curveRecogniser.isEnabled = true
    curveRecogniser.delaysTouchesBegan = true
    firstPage.addGestureRecognizer(curveRecogniser)
    
    let zoomRecogniser = UIPinchGestureRecognizer(target: firstPage, action: Selector(("zoomWithPinch:")))
    firstPage.addGestureRecognizer(zoomRecogniser)

    super.init(coder: aDecoder)

    guard device != nil else { return }

    backgroundColor = UIColor(white: 0.666, alpha: 1.0)
    pages.append(firstPage)
    addSubview(pages[0])
  }

  override func awakeFromNib() {
    
  }

  override func draw(_ rect: CGRect) {
    //os_log("UIBezierPath supports Secure Coding: %{bool}d", log: boardLog, type: .debug, UIBezierPath.supportsSecureCoding)
  }

  override func layoutSubviews() {
    pages[0].frame = layoutPage(pos: 0, nbPages: 1)
  }

  /// For now just one page
  ///
  /// - Parameters:
  ///   - pos: position of the pages in the book
  ///   - nbPages: number of pages in the book
  func layoutPage(pos: Int, nbPages: Int) -> CGRect {
    let pageFormat = pages[pos].pageFormat
    let scale: CGFloat = 1.00
    let safeframe = bounds.inset(by: safeAreaInsets)
    func center(width: CGFloat, height: CGFloat, inset: CGSize) -> CGRect {
      let left = safeframe.origin.x + (safeframe.width - width) / 2.0
      let bottom = safeframe.origin.y + (safeframe.height - height) / 2.0
      return CGRect(x: left+inset.width, y: bottom+inset.height, width: width-2.0*inset.width, height: height-2.0*inset.height)
    }
    if safeframe.width > safeframe.height {
      let height = safeframe.height * scale
      let width = (height * pageFormat.width) / pageFormat.height
      return center(width: width, height: height, inset: margin)
    }
    else {
      let height = safeframe.height * scale
      let width = height * pageFormat.width / pageFormat.height
      return center(width: width, height: height, inset: margin)
    }
  }

  @IBAction func toggleGrid(_ sender: Any) {
    pages[current].showGrid = !pages[current].showGrid
  }

  @IBAction func toggleShowInfoPoint(_ sender: Any) {
    pages[current].showInfoPoint = !pages[current].showInfoPoint
  }
  
  @IBAction func toggleHandleCoalesce(_ sender: Any) {
    curveRecogniser.handleCoalesced = !curveRecogniser.handleCoalesced
    NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":curveRecogniser.handleCoalesced ? "Handle Coalesced" : "Does not handle Coalesced"])
  }
  
  @IBAction func clearPage(_ sender: Any) {
    pages[current].clearPage()
    NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"Page cleared"])
  }
  
  override var canBecomeFirstResponder: Bool {
    return true
  }

  override func didMoveToWindow() {
    becomeFirstResponder()
  }

}
