//
//  ViewController.swift
//  Genesis
//
//  Created by Gerard Iglesias on 27/08/2017.
//  Copyright © 2017 Gerard Iglesias. All rights reserved.
//

import UIKit
import os

class BoardViewController: UIViewController {
  
  lazy var container = {return (UIApplication.shared.delegate)?.sharedBoardContainer()}()

  @IBOutlet weak var boardView: BoardView!
  @IBOutlet weak var infoView: UIView!
  
  var infoViewController: BoardInfoViewController?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let model = container?.managedObjectModel {
      // here extract some features
      os_log("managed object: %@", log: boardLog, type: .debug, model)
    }
    infoView.alpha = 0
    view.bringSubviewToFront(infoView)
    
    infoViewController = children.first { $0.isMember(of: BoardInfoViewController.self) } as? BoardInfoViewController
    infoViewController?.boardViewController = self
    infoViewController?.addMessage(msg: "Started")
    
    boardView.container = container
    boardView.toggleHandleCoalesce(self)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func showInfoView(_ sender: UITapGestureRecognizer) {
    UIViewPropertyAnimator(duration: 0.6, curve: UIView.AnimationCurve.easeIn) {
      self.infoView.alpha = 1 - self.infoView.alpha
      }.startAnimation()
  }

}
