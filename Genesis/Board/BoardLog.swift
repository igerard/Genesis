//
//  BoardLog.swift
//  Genesis
//
//  Created by Gerard Iglesias on 07/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import os
import UIKit

let boardLog = OSLog(subsystem: "com.visual-science.Genesis", category: "BoardPages")
