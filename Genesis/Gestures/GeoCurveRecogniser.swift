//
//  GeoCurveRecogniser.swift
//  Genesis
//
//  Created by Gerard Iglesias on 26/05/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass
import os
import simd


// search/replace
// gesture_log_debug\((".*")(.*)\)
// //os_log($1, log: gestureLog, type: .debug$2)


class GeoCurveRecogniser: UIGestureRecognizer {
  
  var dataPoints = [TouchGeometry]()
  var currentTouch : UITouch? = nil
  var multiTouch = false
  var handleCoalesced = false

  var expectingUpdate = [NSNumber:UITouch.Properties]()

  override var numberOfTouches: Int {
    return 1
  }
  
  fileprivate func trace(_ properties: UITouch.Properties,_ msg: String) {
    if properties.contains(.location) { debugPrint("Location \(msg)")}
    if properties.contains(.force) { debugPrint("Force \(msg)")}
    if properties.contains(.azimuth) { debugPrint("Azimut \(msg)")}
    if properties.contains(.altitude) { debugPrint("Altitude \(msg)")}
  }

  fileprivate func traceEstimatedInfo(_ touch: UITouch) {
    switch touch.estimationUpdateIndex {
    case .some(let index):
      os_log("Estimated : %d", log: gestureLog, type: .debug, index.int64Value)
      [(touch.estimatedProperties, "estimated"),
       (touch.estimatedPropertiesExpectingUpdates, "expected")].forEach{trace($0.0, $0.1)}
    default:
      os_log("Estimated : Final values", log: gestureLog, type: .debug)
    }
  }

  /// Given an UITouch, extract the geometry information we need to handle the position and the different properties
  ///
  /// - Parameter touch: the touch
  /// - Returns: a TouchGeoenetry object
  fileprivate func extractGeometry(touch: UITouch) -> TouchGeometry {
    // compute Y axis inverted position
    let posInView = touch.location(in: view)
    let invertedPosInView = float2(x: Float(posInView.x), y: Float(view!.bounds.size.height-posInView.y))

    //os_log("Extract Geometry with index : %d", log: gestureLog, type: .debug, touch.estimationUpdateIndex?.int64Value ?? Int64(-1))
//    //os_log(type: .debug, "Extract Geometry with index : %d", touch.estimationUpdateIndex?.int64Value ?? Int64(-1))

    switch touch.type {
    case .pencil:
      if let index = touch.estimationUpdateIndex {
        //traceEstimatedInfo(touch)
        expectingUpdate[index] = touch.estimatedPropertiesExpectingUpdates
      }
      let azimV = touch.azimuthUnitVector(in: view)
      // do the Y axis swap on the fly for the aimuth angle and vector
      return TouchGeometry.pencil(pos: invertedPosInView,
                                  force: Float(touch.force),
                                  altAngle: Float(touch.altitudeAngle),
                                  azimAngle: Float(2 * .pi - touch.azimuthAngle(in: view)),
                                  azimVector: float2(Float(azimV.dx), -Float(azimV.dy)),
                                  estimateIndex: touch.estimationUpdateIndex)
    default:
      return TouchGeometry.finger(pos: invertedPosInView)
    }
  }

  fileprivate func extractGeometry(touches: [UITouch]) -> [TouchGeometry] {
    return touches.map{extractGeometry(touch: $0)}
  }

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent){
    let nbTouches = touches.count
    let sameTouch = dataPoints.isEmpty || (nbTouches == 1 && touches.first == currentTouch)
    if  nbTouches > 1 || !sameTouch {
      state = UIGestureRecognizer.State.cancelled
      multiTouch = true
      //os_log("Other touch received.", log: gestureLog, type: .debug)
      return
    }
    //os_log("Curve capture start.", log: gestureLog, type: .debug)
    currentTouch = touches.first
    state = .possible

    switch currentTouch?.type {
    case .some(.pencil):
      //    let locations = touches.map{ $0.location(in: view)}.reduce(""){$0 + $1.debugDescription}
      //    NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"GeoCurveRecogniser possible at : \(locations)"])      
      if let page = view as? PolylineReceiver, let touch = currentTouch {
        dataPoints.append(extractGeometry(touch: touch))
        page.startCurveWith(dataPoints[0])
      }
    default:
      state = UIGestureRecognizer.State.cancelled
      return
    }
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
    if touches.count > 1 || !touches.contains(currentTouch!) {
      return
    }
    state = .changed

    //    let locations = touches.map{ $0.location(in: view)}.reduce(""){$0 + $1.debugDescription}
    //    NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"GeoCurveRecogniser moved at : \(locations)"])

    if let page = view as? PolylineReceiver, let touch = currentTouch {
      let points = handleCoalesced ? extractGeometry(touches: event.coalescedTouches(for: touch) ?? [touch]) : [extractGeometry(touch: touch)]
      dataPoints.append(contentsOf: points)
      //os_log("Coalesced count %d", log: gestureLog, type: .debug, event.coalescedTouches(for: touch)?.count ?? -1)
      page.continueCurveWith(points)
    }
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
    if touches.count > 1 || !touches.contains(currentTouch!) {
      return
    }

    //    let locations = touches.map{ $0.location(in: view)}.reduce(""){$0 + $1.debugDescription}
    //    NotificationCenter.default.post(name: InfoEventGeneric, object: self, userInfo: ["message":"GeoCurveRecogniser ended at : \(locations)"])
    if let page = view as? PolylineReceiver, let touch = currentTouch {
      let points = handleCoalesced ? extractGeometry(touches: event.coalescedTouches(for: touch) ?? [touch]) : [extractGeometry(touch: touch)]
      //os_log("Coalesced count %d", log: gestureLog, type: .debug, event.coalescedTouches(for: touch)?.count ?? -1)
      page.endCurveWith(points)
    }

    state = .ended
  }

  fileprivate func updateFor(_ currentExpected: UITouch.Properties, _ touch: (UITouch), _ index: NSNumber) -> (UITouch.Properties, TouchUpdateGeometry) {
    let newUpdatedExpected = touch.estimatedPropertiesExpectingUpdates
    let properties = currentExpected.subtracting(newUpdatedExpected)
    var position: CGPoint? = .none
    if properties.contains(.location) {
      position = touch.location(in: view)
    }
    var force: CGFloat? = .none
    if properties.contains(.force) {
      force = touch.force
    }
    var azimut: CGFloat? = .none
    var azimutVector: CGVector? = .none
    if properties.contains(.azimuth) {
      azimut = touch.azimuthAngle(in: view)
      azimutVector = touch.azimuthUnitVector(in: view)
    }
    var altitude: CGFloat? = .none
    if properties.contains(.altitude) {
      altitude = touch.altitudeAngle
    }

    return (newUpdatedExpected, TouchUpdateGeometry(pos: position, force: force, altAngle: altitude, azimAngle: azimut, azimVector: azimutVector, estimateIndex: index))
  }

  override func touchesEstimatedPropertiesUpdated(_ touches: Set<UITouch>) {
    let page = view as? PolylineReceiver
    guard page != nil else {
      return
    }
    // see diff before and now
    page?.updateGeometryWith(touches.compactMap{ touch in
      if let index = touch.estimationUpdateIndex, let currentExpected = expectingUpdate[index] {
        let (newExpected, update) = updateFor(currentExpected, touch, index)
        expectingUpdate[index] = newExpected
        return update
      }
      //os_log("Touch update for not registered touch %d (Handle coalesced touch needed)", log: gestureLog, type: .debug, touch.estimationUpdateIndex!.int64Value)
      return .none
    })
  }

  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    //os_log("CANCELLED : %@", log: gestureLog, type: .debug, touches.debugDescription)
    if touches.count == 1 || touches.contains(currentTouch!) {
      state = .failed
      dataPoints.removeAll()
      currentTouch = nil
    }
  }

  override func reset() {

    //os_log("RESET : %@ %{bool}d", log: gestureLog, type: .debug, state.debugDescription, multiTouch)

    dataPoints.removeAll()
    currentTouch = nil
    multiTouch = false
    super.reset()
  }
}
