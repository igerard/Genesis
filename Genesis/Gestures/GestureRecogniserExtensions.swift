//
//  GestureRecogniserExtensions.swift
//  Genesis
//
//  Created by Gerard Iglesias on 06/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import UIKit

extension UIGestureRecognizer.State {
  var debugDescription : String {
    switch self {
    case .began:
      return "began"
    case .cancelled:
      return "cancelled"
    case .changed:
      return "changed"
    case .ended:
      return "ended"
    case .failed:
      return "failed"
    case .possible:
      return "possible"
    }
  }
}

