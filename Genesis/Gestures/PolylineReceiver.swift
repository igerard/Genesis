//
//  RecognizeCurveViewProtocol.swift
//  Genesis
//
//  Created by Gerard Iglesias on 17/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import Foundation

protocol PolylineReceiver {

  /// Start a curve with a TouchGeometry data
  ///
  /// - Parameter point: the TouchGeometry info
  func startCurveWith(_ point: TouchGeometry)

  /// continue a curve with a TouchGeometry data
  ///
  /// - Parameter point: the TouchGeometry info
  func continueCurveWith(_ points: [TouchGeometry])

  /// End a curve with a TouchGeometry data
  ///
  /// - Parameter point: the TouchGeometry info
  func endCurveWith(_ points: [TouchGeometry])

  /// Called to update geometry of the pencil information
  /// Corresponding to the given index in the structure
  ///
  /// - Parameter info: the data to update are not .none
  func updateGeometryWith(_ info: [TouchUpdateGeometry])
}
