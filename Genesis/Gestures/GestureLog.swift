//
//  GestureLog.swift
//  Genesis
//
//  Created by Gerard Iglesias on 06/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import os
import UIKit

let gestureLog = OSLog(subsystem: "com.visual-science.Genesis", category: "CurveRecogniser")
