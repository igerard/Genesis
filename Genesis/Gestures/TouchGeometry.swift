//
//  TouchGeometry.swift
//  Genesis
//
//  Created by Gerard Iglesias on 06/06/2018.
//  Copyright © 2018 Gerard Iglesias. All rights reserved.
//

import UIKit
import simd

/// Enum to memorize information about UITouch geometry handling
///
/// - finger: Data when the finger is used to interact
/// - pencil: Data when the Apple Pencil us used to interect
enum TouchGeometry {
  case finger(pos: float2)
  case pencil(pos: float2, force: Float, altAngle: Float, azimAngle: Float, azimVector: float2, estimateIndex: NSNumber?)
  
  var debugDescription: String {
    switch self {
    case .finger(let pos):
      return String(format: "x -> %.2f y -> %.2f", pos.x, pos.y)
    case .pencil(let pos, let force, let altAngle, let azimAngle, let azimVector, .none):
      return String(format: "x -> %g y -> %g force -> %g altitude -> %g azimuth -> %g / (%g, %g, %g)",
                    pos.x, pos.y, force, altAngle, azimAngle, azimVector.x, azimVector.y)
    case .pencil(let pos, let force, let altAngle, let azimAngle, let azimVector, .some(let index)):
      return String(format: "x -> %g y -> %g force -> %g altitude -> %g azimuth -> %g / (%g, %g, %g) - (%d)",
                    pos.x, pos.y, force, altAngle, azimAngle, azimVector.x, azimVector.y, index)
    }
  }

  func mathPositionString() -> String {
    switch self {
    case .finger(let pos):
      return "{\(pos.x),\(pos.y)}"
    case .pencil(let pos, _, _, _, _, _):
      return "{\(pos.x),\(pos.y)}"
    }
  }
}

/// Simple structure to transmit update information about pencil interaction
struct TouchUpdateGeometry {
  let pos: CGPoint?
  let force: CGFloat?
  let altAngle: CGFloat?
  let azimAngle: CGFloat?
  let azimVector: CGVector?
  let estimateIndex: NSNumber
}

struct TouchGeometryCodableError: Error {
  let msg: String
}

extension TouchGeometry : Codable {
  
  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: String.self)
    let type = try container.decodeIfPresent(Int8.self, forKey: "type")
    switch type {
    case Int8(0):
      let x = try container.decodeIfPresent(Float.self, forKey: "x")!
      let y = try container.decodeIfPresent(Float.self, forKey: "y")!
      self = TouchGeometry.finger(pos: float2(x,y))
    case Int8(1):
      let x = try container.decodeIfPresent(Float.self, forKey: "x")!
      let y = try container.decodeIfPresent(Float.self, forKey: "y")!
      let force = try container.decodeIfPresent(Float.self, forKey: "force")!
      let altAngle = try container.decodeIfPresent(Float.self, forKey: "altAngle")!
      let azimAngle = try container.decodeIfPresent(Float.self, forKey: "azimAngle")!
      let aX = try container.decodeIfPresent(Float.self, forKey: "aX")!
      let aY = try container.decodeIfPresent(Float.self, forKey: "aY")!
      self = TouchGeometry.pencil(pos: float2(x,y), force: force, altAngle: altAngle, azimAngle: azimAngle, azimVector: float2(aX, aY), estimateIndex: .none)

    default:
      throw TouchGeometryCodableError(msg: "Bad data for TouchGeoemtry decoding")
    }
  }

  public func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: String.self)
    switch self {
    case .finger(let pos):
      try container.encode(Int8(0), forKey: "type")
      try container.encode(pos.x, forKey: "x")
      try container.encode(pos.y, forKey: "y")
    case .pencil(let pos, let force, let altAngle, let azimAngle, let azimVector, _):
      try container.encode(Int8(1), forKey: "type")
      try container.encode(pos.x, forKey: "x")
      try container.encode(pos.y, forKey: "y")
      try container.encode(force, forKey: "force")
      try container.encode(altAngle, forKey: "altAngle")
      try container.encode(azimAngle, forKey: "azimAngle")
      try container.encode(azimVector.x, forKey: "aX")
      try container.encode(azimVector.y, forKey: "aY")
    }
  }
  
}

extension String: CodingKey {
  public var stringValue: String {
    return self
  }
  
  public var intValue: Int? {
    return Int(self)
  }
  
  public init?(stringValue: String) {
    self = stringValue
  }
  
  public init?(intValue: Int) {
    self = intValue.description
  }
}
