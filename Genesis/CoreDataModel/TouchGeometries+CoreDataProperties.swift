//
//  TouchGeometries+CoreDataProperties.swift
//  Genesis
//
//  Created by Gerard Iglesias on 16/01/2019.
//  Copyright © 2019 Gerard Iglesias. All rights reserved.
//
//

import Foundation
import CoreData


extension TouchGeometries {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TouchGeometries> {
        return NSFetchRequest<TouchGeometries>(entityName: "TouchGeometries")
    }

    @NSManaged public var sampling: Data?
    @NSManaged public var page: BoardPage?

}
