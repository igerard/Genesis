//
//  BoardPage+CoreDataProperties.swift
//  Genesis
//
//  Created by Gerard Iglesias on 16/01/2019.
//  Copyright © 2019 Gerard Iglesias. All rights reserved.
//
//

import Foundation
import CoreData


extension BoardPage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BoardPage> {
        return NSFetchRequest<BoardPage>(entityName: "BoardPage")
    }

    @NSManaged public var id: String?
    @NSManaged public var geometries: NSSet?

}

// MARK: Generated accessors for geometries
extension BoardPage {

    @objc(addGeometriesObject:)
    @NSManaged public func addToGeometries(_ value: TouchGeometries)

    @objc(removeGeometriesObject:)
    @NSManaged public func removeFromGeometries(_ value: TouchGeometries)

    @objc(addGeometries:)
    @NSManaged public func addToGeometries(_ values: NSSet)

    @objc(removeGeometries:)
    @NSManaged public func removeFromGeometries(_ values: NSSet)

}
