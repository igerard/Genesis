//
//  main.swift
//  Check
//
//  Created by Gerard Iglesias on 17/01/2019.
//  Copyright © 2019 Gerard Iglesias. All rights reserved.
//

import Foundation

class Test {
  let hello: String!
  
  init() {
    hello = nil
  }
  
  func check() {
    print(hello.debugDescription)
  }
}

let h = Test()
h.check()
