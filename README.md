# Genesis
Advanced note taking iPad Pro application

The main objective of this program is to design a very clean and natual UI for a note taking application.

## Context / Objectives

- iOS
- iPad Pro with Apple Pencil
- Swift programming
- Metal based
- Will experiment use of SceneKit for the 3D UI elements
- Use ARKit to allow 3D Scene Note taking
- Smart Drawing

## Functionnal/Technical Specification

### First iteration to discover essential context elements

> Status : Done

- Simple A4 page
- Drawing of pencil samples
- Mastering space coordinate
- use the Pencil position/pressure
- use Metal for the rendering
- integrate a basic trace information UI

### Second interation: Persitence, Experiment Lab and Page Navigation

> Status: In progress

- Create a Data Model for Pages and Geometry
- Use CoreData to persist the model
- Undo & Redo
- Multi Page UI
- Page Grid Navigation 
- UI to navigate into the data
- UI to apply geometry processing on the created drawing
